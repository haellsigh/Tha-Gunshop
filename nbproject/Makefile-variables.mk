#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW-Windows
CND_ARTIFACT_NAME_Debug=tha-gunshop
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW-Windows/tha-gunshop
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW-Windows/package
CND_PACKAGE_NAME_Debug=tha-gunshop.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW-Windows/package/tha-gunshop.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=tha-gunshop
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/tha-gunshop
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=tha-gunshop.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/tha-gunshop.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk

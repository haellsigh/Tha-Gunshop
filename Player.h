/*
 * File:   Player.h
 * Author: Henk
 *
 * Created on January 28, 2015, 7:51 PM
 */

using namespace std;

#include "AbstractWeapon.h"
#include "AbstractEntity.h"
#include <vector>
#include <windows.h>

#ifndef PLAYER_H
#define	PLAYER_H

class Player : public AbstractEntity {

    public:
        Player(AbstractWeapon weapon, string name, unsigned int hp, unsigned int level);

        AbstractWeapon getWeapon(int slot);
        void equip(AbstractWeapon weapon, int slot);

        //Display functions
        void displayWeapons();

        virtual ~Player();

    private:
        int m_maxWeapons;
        std::vector<AbstractWeapon> m_weapons;
};

#endif	/* PLAYER_H */

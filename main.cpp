/*
 * File:   main.cpp
 * Author: Henk
 *
 * Created on January 28, 2015, 5:58 PM
 */

#include "Gun.h"
#include "Player.h"
#include <cstdlib>

using namespace std;

namespace WeaponSlot {
   enum Value { Primary = 0, Secondary = 1, Tertiary = 3 } ;
}

/*
 *
 */
int main(int argc, char** argv) {

    // string name, string description, int damage, double price, int durability, int bullets
    Gun gun1("Test", "A very cool test", 15, 2300.75, 2000, 60);
    Gun gun2("Ak 47", "A very handy gun m9", 30, 4700.00, 1400, 40);

    Player player(gun1, "Wesley 'Hank' de Bruijn", 250, 1);
    player.equip(gun2, WeaponSlot::Secondary);

    AbstractWeapon mainWeapon = player.getWeapon(WeaponSlot::Primary);
    AbstractWeapon secondaryWeapon = player.getWeapon(WeaponSlot::Secondary);

    Player.displayWeapons();

    return 0;
}

/* 
 * File:   Gun.cpp
 * Author: Henk
 * 
 * Created on January 28, 2015, 6:05 PM
 */

#include "Gun.h"

Gun::Gun(string name, string description, int damage, double price, int durability, int bullets) 
    : AbstractWeapon(name, description, damage, price, durability), m_bullets(bullets) {}

int Gun::getBullets() {
    return Gun::m_bullets;
}

// Destructor
Gun::~Gun() {
}


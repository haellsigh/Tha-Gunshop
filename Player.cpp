/*
 * File:   Player.cpp
 * Author: Henk
 *
 * Created on January 28, 2015, 7:51 PM
 */

#include "Player.h"

Player::Player(AbstractWeapon weapon, string name, unsigned int hp, unsigned int level)
    : AbstractEntity(weapon, name, hp, level) {

    // Set max weapons
    m_maxWeapons = GetPrivateProfileInt("Player", "maxWeaponCount", 25, "../Tha-Gunshop/config.ini");
    m_weapons[m_maxWeapons];

    // Add default weapon
    m_weapons.insert(m_weapons.begin(), weapon);
}

void Player::equip(AbstractWeapon weapon, int slot) {

    // If you try to get a slot that doesnt exist
    if(slot > m_maxWeapons) {
        cout << "Slot non existing, there are only " << m_maxWeapons << " slots";
        return;
    }

    // TODO: Message if you have a weapon equipped in that slot and want to swap it, this is apparently not the correct way to do it
    /*AbstractWeapon *oldweapon;
    oldweapon = &m_weapons[slot];

    if(oldweapon != NULL) {
        cout << "test";
    }*/

    m_weapons.insert(m_weapons.begin() + slot, weapon);
    cout << "Weapon " << weapon.getName() << " succesfully equipped";
}

void Player::displayWeapons() {
    std::cout << std::endl;
    for(int i = 0; i < m_maxWeapons; i++)
        std::cout << "Weapon " << i << ": "<< m_weapons[i].getName() << std::endl;
    std::cout << std::endl;
}

AbstractWeapon Player::getWeapon(int slot) {
    return (slot > m_maxWeapons) ? m_weapons[0] : m_weapons[slot];
    /*switch(slot) {
        case 0:
            return m_weapons[0];
            break;
        case 1:
            return m_weapons[1];
            break;
        case 2:
            return m_weapons[2];
            break;
        default:
            return m_weapons[0];
            break;
    }*/
}

Player::~Player() {
}

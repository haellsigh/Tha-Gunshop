/* 
 * File:   AbstractEntity.cpp
 * Author: Henk
 * 
 * Created on January 28, 2015, 7:42 PM
 */

#include "AbstractEntity.h"

AbstractEntity::AbstractEntity(AbstractWeapon weapon, string name, unsigned int hp, unsigned int level) 
    : m_weapon(weapon), m_name(name), m_hp(hp), m_level(level) {}

// Getters
string AbstractEntity::getName(){ return m_name; }
unsigned int AbstractEntity::getHp() { return m_hp; }
unsigned int AbstractEntity::getLevel() { return m_level; }
AbstractWeapon AbstractEntity::getWeapon() { return m_weapon; }

// Setters
void AbstractEntity::setHp(unsigned int hp) { m_hp = hp; }
void AbstractEntity::setLevel(unsigned int level) { m_level = level; }

AbstractEntity::~AbstractEntity() {
}


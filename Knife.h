/* 
 * File:   Knife.h
 * Author: Henk
 *
 * Created on January 28, 2015, 6:55 PM
 */

#include "AbstractWeapon.h"

using namespace std;

#ifndef KNIFE_H
#define	KNIFE_H

class Knife : public AbstractWeapon {
    public:
        Knife(string name, string description, int damage, double price, int durability);
        
        void stab();

        // Destructor
        virtual ~Knife();
    private:

};

#endif	/* KNIFE_H */


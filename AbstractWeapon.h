/* 
 * File:   AbstractWeapon.h
 * Author: Henk
 *
 * Created on January 28, 2015, 5:59 PM
 */

using namespace std;

#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>

#ifndef ABSTRACTWEAPON_H
#define	ABSTRACTWEAPON_H

class AbstractWeapon {
    
    public:
        
        AbstractWeapon(string name, string description, unsigned int damage, double price, unsigned int durability);
        virtual ~AbstractWeapon();
        
        // Getters
        string getName();
        string getDescription();
        unsigned int getDamage();
        unsigned int getDurability();
        double getPrice();
        bool isEmpty();
        
        // Setters
        void setDamage(unsigned int damage);
        void setDurability(unsigned int durability);
        
    private:
        
        string m_name;
        string m_description;
        unsigned int m_damage;
        double m_price;
        unsigned int m_durability;
};

#endif	/* ABSTRACTWEAPON_H */


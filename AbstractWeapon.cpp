/* 
 * File:   AbstractWeapon.cpp
 * Author: Henk
 * 
 * Created on January 28, 2015, 5:59 PM
 */

#include "AbstractWeapon.h"

AbstractWeapon::AbstractWeapon(string name, string description, unsigned int damage, double price, unsigned int durability) 
    : m_name(name), m_damage(damage), m_price(price), m_durability(durability), m_description(description) {}

// Getters
string AbstractWeapon::getName() { return m_name; }
string AbstractWeapon::getDescription() { return m_description; }
unsigned int AbstractWeapon::getDamage() { return m_damage; }
unsigned int AbstractWeapon::getDurability() { return m_durability; }
double AbstractWeapon::getPrice() { return m_price; }
bool AbstractWeapon::isEmpty() { if(m_name.size() > 0) { return false; } return false; };

// Setters
void AbstractWeapon::setDamage(unsigned int damage) { m_damage = damage; }
void AbstractWeapon::setDurability(unsigned int durability) { m_durability = durability; }

AbstractWeapon::~AbstractWeapon() {
}

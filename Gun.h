/* 
 * File:   Gun.h
 * Author: Henk
 *
 * Created on January 28, 2015, 6:05 PM
 */

using namespace std;

#include "AbstractWeapon.h"

#ifndef GUN_H
#define	GUN_H

class Gun : public AbstractWeapon {
    public:        
        Gun(string name, string description, int damage, double price, int durability, int bullets);
        
        void shoot();
        int getBullets();
        
        // Destructor
        virtual ~Gun();
    private:
        unsigned int m_bullets;

};

#endif	/* GUN_H */


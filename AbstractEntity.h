/* 
 * File:   AbstractEntity.h
 * Author: Henk
 *
 * Created on January 28, 2015, 7:42 PM
 */

using namespace std;

#include "AbstractWeapon.h"

#ifndef ABSTRACTENTITY_H
#define	ABSTRACTENTITY_H

class AbstractEntity {
    
    public:
        AbstractEntity(AbstractWeapon weapon, string name, unsigned int hp, unsigned int level);
        virtual ~AbstractEntity();
               
        // Getters
        AbstractWeapon getWeapon();
        string getName();
        unsigned int getHp();
        unsigned int getLevel();
        
        // Setters
        void setHp(unsigned int hp);
        void setLevel(unsigned int level);
        
    private:
        AbstractWeapon m_weapon;
        string m_name;
        unsigned int m_hp;
        unsigned int m_level;
        
        
};

#endif	/* ABSTRACTENTITY_H */


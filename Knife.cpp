/* 
 * File:   Knife.cpp
 * Author: Henk
 * 
 * Created on January 28, 2015, 6:55 PM
 */

#include "Knife.h"

Knife::Knife(string name, string description, int damage, double price, int durability) 
    : AbstractWeapon(name, description, damage, price, durability) {}

Knife::~Knife() {
}

